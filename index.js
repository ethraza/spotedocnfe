/**
 * Spot-eDoc.NFe cloud service Node.js module
 * 
 * Seach and grab NF-e documents by key.
 * Returns the result for each NF-e as a parameter to the callback function.
 * 
 * 
 * @param  {any} oParams Parameters object. Defaults: 
 *  {
 *   "user": "",				// Spot-eDoc.NFe service login user 
 *   "pass": "", 				// Spot-eDoc.NFe service login password
 *   "debug": false, 			// True will verbosely log out messages. 
 *   "returnXmlString": false, 	// True returs the XML message string. False, returns an object of the XML parsed with xml2js.
 *   "logger": "strong-logger",	// If the module is not found, it defaults to console
 *   "url": "http://nfe.spotapp.mobi/xml.php"
 *  }
 * @param  {any} aNfe Array of NF-e keys
 * @param  {any} fCallback Callback function (Error, XML_Responses, Is_a_Good_NFe (null,true,false))
 */
module.exports = function(oParams, aNfe, fCallback) {
	var http = require('http'),
		querystring = require('querystring'),
		xml2js = require('xml2js'),
		async = require('async'),
		oParams = oParams || {},
		url = oParams.url || 'http://nfe.spotapp.mobi/xml.php',
		log;
	
	try {
		log = require(oParams.logger || 'strong-logger');
	} catch (e) {
		log = console;
	}
	
	if (oParams.debug) { log.debug(arguments); }
	
	if (!oParams.user || !oParams.pass) {
		log.error(new Error('Minimun parameters is an Object like {"user": "USER", "pass": "PASSWORD"}'));
		return false;
	}
	
	if (!aNfe || !aNfe[0] || !(aNfe[0].length == 44)) {
		log.error(new Error('We need at least one NFe key in the search array'));
		return false;
	}
	
	if (typeof fCallback !== 'function') {
		log.error(new Error('We need a callback Function to pass error and response'));
		return false;
	}
	
	log.info('Accessing Spot-eDoc.NFe webservice for ' + aNfe.length + ' documents.');
	
	async.each(aNfe, function(k) {
		oParams.key = k;
		
		var req = http.get(url + '?' + querystring.stringify(oParams), function(res) {
			var msg = '';
			
			if (oParams.debug) { log.debug(res); }
			
			res.on('data', function(chunk) {
				msg += chunk;
				
				if (oParams.debug) { log.debug(msg.length); }
			});
			
			res.on('end', function() {
				if (oParams.debug) { log.debug(msg); }
				
				var xml = xml2js.parseString(msg, function(err, xml) {
					var goodNFe;
					
					if (!err) {
						if (xml.nfeProc && xml.nfeProc.NFe[0] && xml.nfeProc.NFe[0].infNFe[0]) {
							if (xml.nfeProc.NFe[0].infNFe[0].Erro) {
								goodNFe = false;
								log.warn(xml.nfeProc.NFe[0].infNFe[0].Erro);
							} else {
								goodNFe = true;
								if (oParams.debug) { log.debug(xml); }
							}
						} else {
							log.warn('Unknown XML response');
						}
						
						fCallback(err, (oParams.returnXmlString == true)? msg : xml, goodNFe);
					} else {
						log.error(new Error('Error parsing XML response (HTTP ' + res.statusCode + '): ' + err.message));
						fCallback(err);
					}
				});
			});
		});
		
		req.on('error', function(e) {
			log.error(new Error('HTTP request error: ' + e.message));
			fCallback(e);
		});
	});
}