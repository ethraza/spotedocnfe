## Synopsis

 Spot-eDoc.NFe cloud service Node.js module.
 
 https://spotapp.mobi
  
 Seach and grab NF-e documents by key.
 
## Code Example

var spotedocnfe = require('spotedocnfe'),
	params = {"user": "USER", "pass": "PASSWORD"},
	nfeKeys = [12345678901234567890123456789012345678901234];

spotedocnfe(params, nfeKeys, function(err, xml, goodNfe) {
	if (!err) {
		console.info(goodNfe, xml);
	} else {
		log.error(new Error(err));
	}
});

## API Reference

 Returns the result for each NF-e as a parameter to the callback function.
  
 @param  {any} oParams Parameters object. Defaults: 
  {
   "user": "",				// Spot-eDoc.NFe service login user 
   "pass": "", 				// Spot-eDoc.NFe service login password
   "debug": false, 			// True will verbosely log out messages. 
   "returnXmlString": false, 	// True returs the XML message string. False, returns an object of the XML parsed with xml2js.
   "logger": "strong-logger",	// If the module is not found, it defaults to console
   "url": "http://nfe.spotapp.mobi/xml.php"
  }
 @param  {any} aNfe Array of NF-e keys
 @param  {any} fCallback Callback function (Error, XML_Responses, Is_a_Good_NFe (null,true,false))

## Contributors

Developer: Allan Brazute @ SpotApp

## License

ISC